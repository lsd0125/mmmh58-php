<?php
require __DIR__. '/__connect_db.php';

$sql = "SELECT * FROM `categories`";
$rows = $pdo->query($sql)->fetchAll();

$cate = [];

foreach($rows as $r){
    if($r['parent_sid']==0){
        $cate[] = $r;
    }
}

foreach($cate as $k=>$c){
    foreach($rows as $r){
        if($r['parent_sid']==$c['sid']){
            $cate[$k]['children'][] = $r;
        }
    }
}

?>
<?php include __DIR__. '/__html_head.php' ?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <?php foreach($cate as $c): ?>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?= $c['name'] ?>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <?php foreach($c['children'] as $c2): ?>
                    <a class="dropdown-item" href="#<?= $c2['sid'] ?>"><?= $c2['name'] ?></a>
                    <?php endforeach ?>
                </div>
            </li>
            <?php endforeach ?>

        </ul>

    </div>
</nav>


<div class="container">
    <pre><?php // print_r($rows) ?></pre>
    <pre><?php //print_r($cate) ?></pre>
</div>
<?php include __DIR__. '/__scripts.php' ?>
<script>
    let cates = <?= json_encode($cate, JSON_UNESCAPED_UNICODE); ?>;



</script>
<?php require __DIR__. '/__html_foot.php' ?>




