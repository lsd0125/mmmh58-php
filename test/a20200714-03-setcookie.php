<?php
    $mycookie = isset($_COOKIE['mycookie']) ? intval($_COOKIE['mycookie']) : 0;
    $mycookie ++;
    // 設定在 HTTP 檔頭, 要等到 response 後才會生效
    setcookie('mycookie', $mycookie);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div>
    <?php
    // 讀取 cookie, 是從 request 過來的 HTTP 檔頭
    if(isset($_COOKIE['mycookie'])){
        echo $_COOKIE['mycookie'];
    }
    ?>
</div>
</body>
</html>
