<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

</head>
<body>
<?php
    printf('%s * %s = %s <br>', 3, 5, 3*5);

    $s = sprintf('%s * %s = %s <br>', 7, 8, 7*8);

    echo $s;
?>
</body>
</html>
