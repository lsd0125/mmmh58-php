<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<!-- http://localhost/proj58/test/a20200709-02-get.php?name=vic&age=27 -->
<!-- urlencoded 格式 -->
Hello, <?= isset($_GET['name']) ? $_GET['name'] : 'Nobody' ?>
<br>
<?= isset($_GET['age']) ? $_GET['age'] : '' ?>
</body>
</html>
