<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<pre>
<?php

$ar = array(3, 6, 8, 'david');
$ar2 = [3, 6, 8,  'david'];
$ar2[0] = 123;
$ar2[] = 456;

print_r($ar);

var_dump($ar2);

?>
</pre>
</body>
</html>
